// This module returns jam servers

// TODO: Validate all data sent over socket as MIDI

var net = require('net');

var num_connection = function(server, cb) {                // Takes server instance and callback function
  server.getConnections(function(err, count) {
    if (err) throw err;
    cb(count);
  });
};

// This could be an env variable or something idk.
//var midi_server = 'rocket.2013.nodeknockout.com';
var midi_server = '127.0.0.1:8000';

var redir_response = 'HTTP/1.1 200 OK\r\nRefresh: 0; url=http://' + midi_server + '/instructions/'
                   + '\r\nContent-Type: text/html\r\nContent-length: ';

var make_jam = function() {

  var master_server = net.createServer(function(master_connection) {

    //console.log('master server client connected');

    master_connection.on('end', function() {
      //console.log('master client disconnected');
      slave_server.close();
    });

    master_connection.on('data', function(data) {
      // Check for HTTP request and redirect them
      // reference: https://en.wikipedia.org/wiki/HTTP_302

      var check_me = data.toString().substr(0,3);

      // If we get an HTTP GET (browser) redirect them
      if (check_me === 'GET') {

        // send response and content length
        master_connection.end(redir_response + redir_response.length + '\r\n\r\n'); 
      }
    });

    /* MASTER CLIENT HAS CONNECTED */

    // Create a slave server inside the context of the master server...
    var slave_server = net.createServer(function(slave_connection) {

        slave_connection.on('end', function() {
        num_connection(slave_server, function(count) {
          //console.log('Disconnected, ' + count + ' slave clients connected');
          // TODO: update webpage with notification of new slave
        });
      });

      slave_connection.on('data', function(data) {

        var check_me = data.toString().substr(0,3);

        // If we get an HTTP GET (browser) redirect them
        if (check_me === 'GET') {
          slave_connection.end(redir_response + redir_response.length + '\r\n\r\n'); 
        }
      });

      /* SLAVE CLIENT HAS CONNECTED */

      // Pipe master streams to slaves streams...
      master_connection.pipe(slave_connection);

      num_connection(slave_server, function(count) {
        // console.log('slave server client connected, ' + count + ' clients');
        // TODO: update webpage with notification of new slave
      });

    });

    slave_server.on('connection', function(slave_socket) {

      // Send empty byte to signify slave
      slave_socket.write('\x00');
      //console.log('sending slave byte');
    });

    // Our slave server listens on a port above the random port
    slave_server.listen(slave_port);
  });

  // Master Connection event
  master_server.on('connection', function(master_socket) {
    //console.log('sending master byte');

    // Send a full byte to signify master
    master_socket.write('\xFF');
  });

  // And our master server listens on a random port
  master_server.listen(0);

  // Get our port number
  var master_port = master_server.address().port;

  // Add one if it is legal, subtract one from the port otherwise for the slave server
  var slave_port  = (master_port < 65535) ? master_port + 1 : master_port - 1;

  // Here we define our jam object with port numbers and the master server
  var jam = {
    server      : master_server,
    master_port : master_port,
    slave_port  : slave_port
  }

  return jam;
}

// Export our jam maker function
module.exports = make_jam;
