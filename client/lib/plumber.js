// This module is a function that takes:
// 1. the path to a MIDI device
// 2. the address of a server in the format "address:port" (string)
// 
// After creating a file ReadStream and a TCP stream
// it pipes them together, hence ... plumber
//
// It then returns:
// an object container 2 duplex streams, like so:
// {
//   midi: midi,
//   server: server
// }

var net = require('net');
var fs  = require('fs');

var our_streams = {};

var createMidiStream = function(path, role, cb) {
  var midi;

  if (role === 'slave') {

    // Our write stream
    midi = fs.createWriteStream(path);
    our_streams.client.pipe(midi);

  } else if (role === 'master') {

    // Our MIDI read stream
    midi = fs.createReadStream(path);
    midi.pipe(our_streams.client);
  }

  our_streams.midi = midi;

  // Callback to index.js
  cb(our_streams);
}

var plumber = function(midiPath, serverAddress, cb) {

    var streams, role;

    // Get our address/port from string
    serverAddress = serverAddress.split(':');
    var address = serverAddress[0];
    var port = serverAddress[1];

    // stream to server (either master or slave)
    var client = net.createConnection({ host: address, port: port }, function() { 
        console.log('midi-rocket CONNECTed');
    });

    // Our network client stream to be exported
    our_streams.client = client;

    // Read initial byte and figure out slave or master and direction of pipe
    client.once('data', function(data) {
      data = data.toString();

      // \xFF is master, MIDI -----> NET
      if(data === '\xFF') {
        //console.log('MASTER DETECTED');
        role = 'master';
        our_streams.role = role;
        midi = createMidiStream(midiPath, role, cb);

      // \x00 is slave,  MIDI <----- NET
      } else if(data === '\x00') {
        //console.log('SLAVE DETECTED');
        role = 'slave';
        our_streams.role = role;
        midi = createMidiStream(midiPath, role, cb);

      } else {
        console.log('uh oh spaghettios');  // for the lulz it shall forever be uncommented
      }

    });

}

module.exports = plumber;
