var path = require('path');
var fs = require('fs');
var events = require('events');


Detector = function() {};

Detector.prototype = new events.EventEmitter;

Detector.prototype.detect = function() {

        var self = this;
        var matches = [ 'one', 'two', 'three' ];
	var sndDevices = fs.readdir('/dev/snd', function(err, files) {
            if (err) {
                console.log('problem reading your sound devices');
                sndDevices = [ '/dev/snd/dummy0', '/dev/snd/dummy1' ]; // some dummy sound devices so our gui list can be populated for testing purposes
            }

            var patt = new RegExp('midi', 'i');

            var matches = files.filter(function isMatch(file) {
                return patt.exec(file);                                // if array elements match the regex, they are added to the new matches array

            }).map(function(file) {
                var txt = '/dev/snd/'
                return txt.concat(file)
            });

            if (matches) {
                self.emit('detection', matches);

	    } else {
          console.log('No Midi interfaces detected!');
	    }
	});

    return matches;

}



module.exports = Detector;

