var socket = io.connect('localhost');

function blinkLED(type, duration, dir) {
    var on, off;
    var ledSelector = $('#' + type + '-led-' + dir);

    console.log(dir);

    switch (type) {
    case 'sync':
        on = '#0F0';
        off = '#014D05';
        break;
    case 'activity':
        on = '#F00';
        off = '#9B0005';
        break;
    }

    ledSelector.css({
        'background-color': on
    });

    setTimeout(function () {
        ledSelector.css({
            'background-color': off
        });
    }, duration);

}

window.onload = function () {
    
    // Let server know things are loaded
    socket.emit('ready');

    socket.on('connect', function () {
        socket.emit('message', {
            message: 'i am client gui'
        });
        console.log('socket.io established connection');
    });

    socket.on('message', function (data) {
        console.log(data);
        $("#status").html(data.message);
    });

    socket.on('midi', function (data) {
        if (data.midi === "f8") {
            blinkLED('sync', 10, data.dir);
        } else {
            blinkLED('activity', 50, data.dir);
        }
    });

    socket.on('devices', function(data) {
        $("#midi-dropdown").html('<p>RESETTED</p>');  // reset the dropdown box

        $.each(data.devices, function( i, device ) {
            console.log(i, device);
            $("#midi-dropdown").append('<option>' + device + '</option>');
        });
        $('#midi-path').val($("#midi-dropdown option:selected").text());
    });

    // Change the text in the textbox when dropdown changes
    $('#midi-dropdown').change(function() {
      $('#midi-path').val($('#midi-dropdown option:selected').text());
    });


    $("#connect").on('click', function () {
            var midi_path = $('#midi-path').val();
            var address = $('#address-port').val();
            var info = {
                midi_path: midi_path,
                address: address
            };
            console.log('info', info);
            socket.emit('connect', info);


            $("#start").on('click', function () {
                $("#status").html('start is CLIIIIICKd');
                console.log('start CLICK');
                $("#start").html('Play is clicked');
                socket.emit('message', {
                    message: 'start'
                });
            });

            $("#stop").on('click', function () {
                console.log('STOP IS CLICKED!');
                $("#start").html('Stop is clicked');
            });
    });
}
