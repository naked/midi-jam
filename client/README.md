jam-client
==========

This server is run locally and provides an interface to configure which MIDI ports are connected to the server, what type of instruments are connected \
and what their role is in relation to the others.
