// This is the server that runs on the client 
// (connects midi interfaces to the proxy server and runs a GUI)

// INCLUDES
var http = require('http')
var connect = require('connect');
var app = connect();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var rsvp = require('rsvp');
var events = require('events');

var detector = require('./lib/detector.js'); // detects midi devices
var plumber = require('./lib/plumber.js');   // handles piping between slave and master

var PORT = 8080;

var det = new detector();

// Turn down debug messages, default 3
io.set('log level', 0);   // Just errors

server.listen(PORT, function() {
  console.log('\n*******************************************************************************');
  console.log('Point your browser to http://localhost:' + PORT + ' to connect to web interface');
  console.log('*******************************************************************************');
  console.log('Debug Messages:')
});

// Static file server
app.use(connect.static(__dirname + '/public'));


controlSocket = io.sockets.on('connection', function (socket) {
    
    // Seeing if we can prevent the initial page showing nothing by having it emit a 'ready'
    // event back to the server and then we go on and do everything
    socket.on('ready', function() {
      socket.emit('message', { message: "Welcome to MiDI jamzession!" });
      det.detect();

      /** DETECTOR EVENT LISTENERS **/
      det.on('detection', function(data) {

          // when midi devices have been detected, we send the list of devices to the gui
          console.log('detection event has fired');
          controlSocket.emit('devices', { 'devices': data });
      });


      socket.on('message', function(data) {
          console.log(data);
      });

      // This event listener is for when people click that blue connect button
      socket.on('connect', function(data) {

          /** PLUMBER STREAMS (MIDI/Server Network) EVENT LISTENERS **/
          var plumb = plumber(data.midi_path, data.address, function(our_streams) {
          var midi   = our_streams.midi;
          var client = our_streams.client;

          // MIDI LISTENERS
          midi.on('data', function(data) {
              data = data.toString('hex');
              //console.log(data);
              controlSocket.emit('midi', { midi: data, dir: 'out' });
          });

          // Ok, handle the error by letting the user know they did done something wrong
          midi.on('error', function(err) {
            socket.emit('message', { message: "An error has occured, check your midi device path" });
          });

          // NET LISTENERS
          client.on('data', function(data) {
          //    console.log('DATA:' + data.toString());
              data = data.toString('hex');
              controlSocket.emit('midi', { midi: data, dir: 'in' });
          });

          client.on('end', function() {
              //console.log('client disconnect');
          });

          client.on('error', function() {
            controlSocket.emit('message', { message: "An error has occured, make sure the master server has started and you have the correct address and port number for the server" });
          });

        }); /** plumber end **/
      });
    });
});
