// https://github.com/nko4/website/blob/master/module/README.md#nodejs-knockout-deploy-check-ins
require('nko')('4V996zGYDwa4RBSB');
var isProduction = (process.env.NODE_ENV === 'production');
var port = (isProduction ? 80 : 8000);

// Our code starts here
var redis      = require('redis');
var express    = require('express.io');
var nunjucks   = require('nunjucks');

var RedisStore = require('connect-redis')(express);
var store_options = { client: redis.createClient() };

// Include our jam server maker
var jam = require('./lib/jam_maker.js');

var app = express().http().io();


app.set('views', __dirname + '/views');

app.use(express.static(__dirname + '/public'));    // Serve our static files
app.use(express.cookieParser());
app.use(express.session({ store: new RedisStore(store_options), secret: 'shhhhhhh' }));


// Setting up nunjucks templating
nunjucksEnv = new nunjucks.Environment( new nunjucks.FileSystemLoader(__dirname + '/views'), {
      autoescape: true
});

nunjucksEnv.express(app);

/** ROUTES **/
app.get('/', function(req, res) {
  res.render('index.html');
});

app.get('/instructions', function(req, res) {
  res.render('instructions.html');
});

app.get('/README', function(req, res) {
  res.render('README.html');
});

app.io.route('create_jam', function(req) {

  if (!req.session.jam) {
    var my_jam = jam();
    req.session.jam = my_jam;
  } else {
    var my_jam = req.session.jam;
  }

  var jam_info = {
    master_port : my_jam.master_port,
    slave_port  : my_jam.slave_port
  }

  req.io.emit('served_jam', jam_info);

});

// Our code ends here
app.listen(port, function(err) {
  if (err) { console.error(err); process.exit(-1); }

  // if run as root, downgrade to the owner of this file
  if (process.getuid() === 0) {
    require('fs').stat(__filename, function(err, stats) {
      if (err) { return console.error(err); }
      process.setuid(stats.uid);
    });
  }

  console.log('Server running at http://0.0.0.0:' + port + '/');
});

// This server should be for the status page
