MIDI Jam
===============

*   Alice and Bob both have MIDI sequencers.
*   Alice and Bob would like to sync their instruments, but they don't have a 2000 mile long MIDI cable.
![Alice and Bob](http://i.imgur.com/X77x7Jm.png)
*   With MIDI Jam they can create a MIDI connection over the internet.

Here's how:
--------------

1.  Both Alice and Bob download the Node.JS client software and configure it to read their MIDI interfaces.
2.  Alice goes to http://rocket.2013.nodeknockout.com/ and creates a jam room and sends bob the URL over IM
3.  Alice connects her client to the "Master connection" and she will be sending the MIDI sync signal (and all other MIDI signals) to the midi slaves that connect.
4.  Bob connects his client to the "Slaves connection" and his computer's MIDI output is now streaming Alice's signals to his setup!
5.  When Cathy IMs Bob asking to join, he sends her the URL and she uses the Slaves connection to also begin using Alice's MIDI signals.


They each individually record their tracks and send the files to eachother to mix themselves.

![Alice and Bob](http://i.imgur.com/OJjhc8y.png)

*Well done!*
