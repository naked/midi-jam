// Our home.js file for the index page
var socket = io.connect();
var midi_server = 'rocket.2013.nodeknockout.com';   // Server for our MIDI client

// On ready wrapper
$(function() {

  // When someone clicks our big create jam button...
  $('#create-jam').click(function() {

    // Emit create_jam route request
    socket.emit('create_jam');
  });

  $('.copy-me').click(function() {

    // This should make it easier to highlight, just selects the box after 50ms
    (function(that) {
      setTimeout(function() {
        $(that).focus().select();
      }, 50);
    }(this));

  });

  // Listener for when the server has created out Jam session
  socket.on('served_jam', function(jam_info) {

    // Change to information screen
    // TODO: Should this  be re-routing to jam-room page? I think so
    $('#intro-screen').fadeOut('fast', function() {
      $('#information-screen').fadeIn('fast');
    });

    $('#master-port').val(midi_server + ":" + jam_info.master_port);
    $('#slave-port').val(midi_server + ":" + jam_info.slave_port);
  });

});
